import React from 'react';
import PropTypes from 'prop-types';
import { Segment, Form, Header } from 'semantic-ui-react';
import { Map } from 'immutable';
import FormField from 'components/FormField';


class Component extends React.Component {
  static propTypes = {
    onValueChange: PropTypes.func.isRequired,
    onRegisterClick: PropTypes.func.isRequired,
    email: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired,
    confirm: PropTypes.string.isRequired,
    errors: PropTypes.instanceOf(Map),
  }

  static defaultProps = {
    errors: Map(),
  }

  componentDidMount() {}

  render() {
    const {
      email,
      password,
      confirm,
      errors,
      onValueChange,
      onRegisterClick,
    } = this.props;
    return (
      <Segment>
        <Form>
          <Header
            content="Register"
          />
          <FormField
            label="Email"
            fieldName="email"
            placeholder="Enter your email..."
            tabIndex="0"
            value={email}
            errors={errors.get('email')}
            onChange={onValueChange}
          />
          <FormField
            label="Password"
            fieldName="password"
            type="password"
            tabIndex="0"
            value={password}
            errors={errors.get('password')}
            onChange={onValueChange}
          />
          <FormField
            label="Confirm password"
            fieldName="confirm"
            type="password"
            tabIndex="0"
            value={confirm}
            errors={errors.get('confirm')}
            onChange={onValueChange}
          />
          <Form.Button
            onClick={onRegisterClick}
            content="Register"
          />
        </Form>
      </Segment>
    );
  }
}

export default Component;

