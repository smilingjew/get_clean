const getEmail = ({ register }) => register.email;
const getPassword = ({ register }) => register.password;
const getErrors = ({ register }) => register.errors;
const getConfirm = ({ register }) => register.confirm;

export default {
  getEmail,
  getPassword,
  getErrors,
  getConfirm,
};
