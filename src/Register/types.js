const PREFIX = '$$REGISTER_/';

export default {
  SET_REGISTER_VALUE: `${PREFIX}SET_LOGIN_VALUE`,
  SET_ERRORS: `${PREFIX}SET_ERRORS`,
};
