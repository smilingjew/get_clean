import { connect } from 'react-redux';
import Component from 'Register/Component';
import actions from 'Register/actions';
import selectors from 'Register/selectors';


const mapStateToProps = state => ({
  email: selectors.getEmail(state),
  password: selectors.getPassword(state),
  confirm: selectors.getConfirm(state),
  errors: selectors.getErrors(state),
});

const mapDispatchToProps = dispatch => ({
  onValueChange: (fieldName, value) => { dispatch(actions.setRegisterValue(fieldName, value)); },
  onRegisterClick: () => { dispatch(actions.register()); },
});


export default connect(mapStateToProps, mapDispatchToProps)(Component);
