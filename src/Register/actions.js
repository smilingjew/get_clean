import { fromJS } from 'immutable';
import types from 'Register/types';
import selectors from 'Register/selectors';
import authActions from 'auth/actions';

const setRegisterValue = (fieldName, value) => ({
  type: types.SET_REGISTER_VALUE,
  payload: {
    fieldName,
    value,
  },
});

const setErrors = errors => ({
  type: types.SET_ERRORS,
  payload: {
    errors,
  },
});

const register = () => async (dispatch, getState, api) => {
  const state = getState();
  dispatch(setErrors(fromJS({})));
  const email = selectors.getEmail(state);
  const password = selectors.getPassword(state);
  const confirm = selectors.getConfirm(state);
  let resp;
  try {
    resp = await api.registerUser(email, password, confirm);
  } catch (error) {
    ({ response: resp } = error);
  }
  if (resp.status < 300) {
    await dispatch(authActions.loadUser(resp.data));
    return;
  }

  if (resp.data.error) {
    const errors = fromJS(resp.data.error);
    dispatch(setErrors(errors));
  }
};

export default {
  setRegisterValue,
  setErrors,
  register,
};
