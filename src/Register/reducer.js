import { Record, Map } from 'immutable';
import types from 'Register/types';

const State = Record({
  email: '',
  password: '',
  confirm: '',
  errors: Map(),
});

export default (state = new State(), action) => {
  switch (action.type) {
    case types.SET_REGISTER_VALUE:
      return state.withMutations((s) => {
        const {
          fieldName,
          value,
        } = action.payload;
        return s.set(fieldName, value);
      });
    case types.SET_ERRORS:
      return state.withMutations((s) => {
        const { errors } = action.payload;
        return s.set('errors', errors);
      });
    default:
      return state;
  }
};
