import { connect } from 'react-redux';
import { push } from 'redux-little-router';
import Component from 'App/Component';
import authSelectors from 'auth/selectors';
import authActions from 'auth/actions';

const mapStateToProps = state => ({
  isLoading: authSelectors.getIsLoadingUser(state),
  isSidebarVisible: authSelectors.getLoggedIn(state),
});

const mapDispatchToProps = dispatch => ({
  onInit: () => {
    dispatch(authActions.initUser());
  },
  onNavClick: (url) => { dispatch(push(url)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
