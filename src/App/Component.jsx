import React from 'react';
import PropTypes from 'prop-types';
import { Fragment } from 'redux-little-router';
import { Sidebar, Container, Menu, Button } from 'semantic-ui-react';
import LogIn from 'LogIn';
import Register from 'Register';
import Upcoming from 'Upcoming';
import NewChore from 'NewChore';
import EditChore from 'EditChore';
import ChoreInstance from 'ChoreInstance';
import ChoreList from 'ChoreList';
import Notifications from 'Notifications';
import Loader from 'components/Loader';


class Component extends React.Component {
  static propTypes = {
    onInit: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    onNavClick: PropTypes.func.isRequired,
    isSidebarVisible: PropTypes.bool.isRequired,
  };

  componentDidMount() {
    this.props.onInit();
  }

  render() {
    const {
      isLoading,
      onNavClick,
      isSidebarVisible,
    } = this.props;

    if (isLoading) {
      return <Loader />;
    }

    return (
      <Sidebar.Pushable className="chore-app">
        <Sidebar
          visible={isSidebarVisible}
          width="very thin"
          compact
          icon
          as={Menu}
          fitted="horizontally"
          vertical
          items={[
              {
                key: 'home',
                content: (
                  <Button
                    circular
                    basic
                    icon="home"
                  />
                ),
                onClick: () => {
                  onNavClick('/');
                },
              },
              {
                key: 'chores',
                content: (
                  <Button
                    circular
                    basic
                    icon="tasks"
                  />
                ),
                onClick: () => {
                  onNavClick('/chores');
                },
              },
          ]}
        />
        <Sidebar.Pusher className="chore-app-content">
          <Container className="app-container">
            <Fragment forRoute="/">
              <div>
                <Fragment forRoute="/">
                  <Upcoming />
                </Fragment>
                <Fragment forRoute="/upcoming/:choreInstanceId">
                  <ChoreInstance />
                </Fragment>
                <Fragment forRoute="/chores/:choreId">
                  <EditChore />
                </Fragment>
                <Fragment forRoute="/chores">
                  <ChoreList />
                </Fragment>
                <Fragment forRoute="/login">
                  <LogIn />
                </Fragment>
                <Fragment forRoute="/register">
                  <Register />
                </Fragment>
                <Fragment forNoMatch>
                  <span>Not Found</span>
                </Fragment>
                <NewChore />
                <Notifications />
              </div>
            </Fragment>
          </Container>
        </Sidebar.Pusher>
      </Sidebar.Pushable>
    );
  }
}

export default Component;
