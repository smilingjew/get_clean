import { connect } from 'react-redux';
import { push } from 'redux-little-router';
import Component from 'Upcoming/Component';
import actions from 'Upcoming/actions';
import selectors from 'Upcoming/selectors';

const mapStateToProps = state => ({
  isLoading: selectors.getIsLoading(state),
  upcomingChores: selectors.getUpcomingChoreList(state),
});

const mapDispatchToProps = dispatch => ({
  onInit: () => { dispatch(actions.init()); },
  onUpcomingClick: (upcomingChore) => { dispatch(push(`/upcoming/${upcomingChore.id}`)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
