import { Record, Map } from 'immutable';
import types from 'Upcoming/types';

const State = Record({
  loading: true,
  upcomingChoreMap: Map(),
});

export default (state = new State(), action) => {
  switch (action.type) {
    case types.SET_UPCOMING_MAP:
      return state.withMutations((s) => {
        const { upcomingMap } = action.payload;
        return s.set('upcomingChoreMap', upcomingMap)
          .set('loading', false);
      });
    default:
      return state;
  }
};
