import { Record } from 'immutable';

const UpcomingChore = Record({
  id: null,
  name: '',
  dueDate: null,
});

export default {
  UpcomingChore,
};
