import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { Segment, Header } from 'semantic-ui-react';
import UpcomingChoreInfo from 'Upcoming/components/UpcomingChoreInfo';
import Loader from 'components/Loader';

class Component extends React.Component {
  static propTypes = {
    onInit: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    upcomingChores: PropTypes.instanceOf(List).isRequired,
    onUpcomingClick: PropTypes.func.isRequired,
  }
  componentDidMount() {
    this.props.onInit();
  }

  render() {
    const {
      isLoading,
      upcomingChores,
      onUpcomingClick,
    } = this.props;

    if (isLoading) {
      return <Loader />;
    }

    return (
      <div>
        <Header
          content="Upcoming"
        />
        <Segment.Group>
          {upcomingChores.map(upcomingChore => (<UpcomingChoreInfo
            upcomingChore={upcomingChore}
            key={upcomingChore.id}
            onActionClick={onUpcomingClick}
          />))}
        </Segment.Group>
      </div>
    );
  }
}

export default Component;
