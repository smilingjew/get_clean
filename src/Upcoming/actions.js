import { Map } from 'immutable';
import types from 'Upcoming/types';
import models from 'Upcoming/models';

const setUpcomingMap = upcomingMap => ({
  type: types.SET_UPCOMING_MAP,
  payload: {
    upcomingMap,
  },
});

const init = () => async (dispatch, getState, api) => {
  const resp = await api.getUpcomingChores();
  let upcomingMap = resp.data.map(uc => [uc.id, new models.UpcomingChore(uc)]);
  upcomingMap = Map(upcomingMap);
  dispatch(setUpcomingMap(upcomingMap));
};

export default {
  init,
};
