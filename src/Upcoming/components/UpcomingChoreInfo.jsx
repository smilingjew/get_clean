import React from 'react';
import PropTypes from 'prop-types';
import { format } from 'date-fns';
import { Segment, Button, Grid } from 'semantic-ui-react';
import models from 'Upcoming/models';

const UpcomingChoreInfo = ({
  upcomingChore,
  onActionClick,
}) => (
  <Segment>
    <Grid>
      <Grid.Row>
        <Grid.Column width="5" verticalAlign="middle" textAlign="left">
          <Button
            content="Do it!"
            onClick={() => { onActionClick(upcomingChore); }}
          />
        </Grid.Column>
        <Grid.Column width="5" verticalAlign="middle" textAlign="center">
          {upcomingChore.name}
        </Grid.Column>
        <Grid.Column width="5" verticalAlign="middle" textAlign="right">
          Due: <span>{format(upcomingChore.dueDate, 'MMMM Do, YYYY')}</span>
        </Grid.Column>
      </Grid.Row>
    </Grid>

  </Segment>
);

UpcomingChoreInfo.propTypes = {
  upcomingChore: PropTypes.instanceOf(models.UpcomingChore).isRequired,
  onActionClick: PropTypes.func.isRequired,
};

export default UpcomingChoreInfo;
