import { createSelector } from 'reselect';

const getIsLoading = ({ upcoming }) => upcoming.loading;
const getUpcomingChoreMap = ({ upcoming }) => upcoming.upcomingChoreMap;

const getUpcomingChoreList = createSelector(
  [getUpcomingChoreMap],
  upcomingChoreMap => upcomingChoreMap.toList().sortBy(uc => -uc.dueDate),
);

export default {
  getIsLoading,
  getUpcomingChoreMap,
  getUpcomingChoreList,
};
