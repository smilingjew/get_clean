import { Record } from 'immutable';

const User = Record({
  id: null,
  email: null,
});

export default {
  User,
};
