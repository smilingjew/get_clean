import { LOCATION_CHANGED, push } from 'redux-little-router';
import selectors from 'auth/selectors';

export default store => next => (action) => {
  if (action.type === LOCATION_CHANGED) {
    if (action.payload.result) {
      const { needsAuth } = action.payload.result;
      if (needsAuth) {
        const loggedIn = selectors.getLoggedIn(store.getState());
        if (!loggedIn) {
          store.dispatch(push('/login'));
          return null;
        }
      }
    }
  }
  return next(action);
};
