import { Record } from 'immutable';
import types from 'auth/types';

const State = Record({
  user: null,
  loadingUser: true,
});

export default (state = new State(), action) => {
  switch (action.type) {
    case types.SET_USER:
      return state.withMutations((s) => {
        const { user } = action.payload;
        return s.set('user', user)
          .set('loadingUser', false);
      });
    default:
      return state;
  }
};
