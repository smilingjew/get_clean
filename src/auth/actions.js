import { push, initializeCurrentLocation } from 'redux-little-router';
import types from 'auth/types';
import models from 'auth/models';

const setUser = user => ({
  type: types.SET_USER,
  payload: {
    user,
  },
});

const loadUser = user => async (dispatch) => {
  await dispatch(setUser(new models.User(user)));
  await dispatch(push('/'));
};

const initUser = () => async (dispatch, getState, api) => {
  const initialLocation = getState().router;
  let resp;
  try {
    resp = await api.loadUser();
  } catch (error) {
    ({ response: resp } = error);
  }
  if (resp.status < 300) {
    await dispatch(setUser(new models.User(resp.data)));
    dispatch(initializeCurrentLocation(initialLocation));
  } else {
    await dispatch(setUser(null));
    dispatch(push('/login'));
  }
};

export default {
  setUser,
  loadUser,
  initUser,
};
