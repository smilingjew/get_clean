const getLoggedIn = ({ auth }) => auth.user !== null;
const getIsLoadingUser = ({ auth }) => auth.loadingUser;

export default {
  getLoggedIn,
  getIsLoadingUser,
};
