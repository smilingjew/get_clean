import React from 'react';
import PropTypes from 'prop-types';
import { Button, Icon } from 'semantic-ui-react';

const Component = ({
  onNewClick,
  show,
}) => (
  show ? (
    <Button
      content="Add Chore"
      className="new-chore-button"
      circular
      icon="plus"
      labelPosition="left"
      onClick={onNewClick}
    />)
    :
    null
);

Component.propTypes = {
  onNewClick: PropTypes.func.isRequired,
  show: PropTypes.bool.isRequired,
};

export default Component;
