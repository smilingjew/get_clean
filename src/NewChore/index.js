import { connect } from 'react-redux';
import { push } from 'redux-little-router';
import Component from 'NewChore/Component';
import selectors from 'NewChore/selectors';

const mapStateToProps = state => ({
  show: selectors.getShowNewChore(state),
});

const mapDispatchToProps = dispatch => ({
  onNewClick: () => { dispatch(push('/chores/new')); },
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
