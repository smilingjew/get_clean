import { createSelector } from 'reselect';
import authSelectors from 'auth/selectors';

const getPath = ({ router }) => router.pathname;

const getShowNewChore = createSelector(
  [getPath, authSelectors.getLoggedIn],
  (path, loggedIn) => {
    if (!loggedIn) {
      return false;
    }

    if (path === '/chores/new') {
      return false;
    }

    if (path !== '/chores' && path.indexOf('/chores') !== -1) {
      return false;
    }

    return true;
  },
);

export default {
  getShowNewChore,
};
