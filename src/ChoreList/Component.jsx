import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { Segment, Header } from 'semantic-ui-react';
import Loader from 'components/Loader';
import ChoreInfo from 'ChoreList/components/ChoreInfo';

class Component extends React.Component {
  static propTypes = {
    loading: PropTypes.bool.isRequired,
    chores: PropTypes.instanceOf(List).isRequired,
    onInit: PropTypes.func.isRequired,
    onChoreClick: PropTypes.func.isRequired,
  }
  componentDidMount() {
    this.props.onInit();
  }

  render() {
    const {
      loading,
      chores,
      onChoreClick,
    } = this.props;

    if (loading) {
      return <Loader />;
    }

    return (
      <div>
        <Header
          content="Chores"
        />
        <Segment.Group>
          {chores.map(chore => (
            <ChoreInfo
              key={chore.id}
              chore={chore}
              onClick={onChoreClick}
            />
          ))}
        </Segment.Group>
      </div>
    );
  }
}

export default Component;

