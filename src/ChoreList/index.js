import { connect } from 'react-redux';
import { push } from 'redux-little-router';
import Component from 'ChoreList/Component';
import actions from 'ChoreList/actions';
import selectors from 'ChoreList/selectors';

const mapStateToProps = state => ({
  loading: selectors.getIsLoading(state),
  chores: selectors.getChores(state),
});

const mapDispatchToProps = dispatch => ({
  onInit: () => { dispatch(actions.init()); },
  onChoreClick: (chore) => { dispatch(push(`/chores/${chore.id}`)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
