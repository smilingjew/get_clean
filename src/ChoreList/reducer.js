import { Record, Map } from 'immutable';
import types from 'ChoreList/types';

const State = Record({
  loading: true,
  choreMap: Map(),
});

export default (state = new State(), action) => {
  switch (action.type) {
    case types.SET_CHORE_MAP:
      return state.withMutations((s) => {
        const { choreMap } = action.payload;
        return s.set('choreMap', choreMap)
          .set('loading', false);
      });
    default:
      return state;
  }
};
