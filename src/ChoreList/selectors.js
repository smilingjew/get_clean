import { createSelector } from 'reselect';

const getIsLoading = ({ choreList }) => choreList.loading;
const getChoreMap = ({ choreList }) => choreList.choreMap;

const getChores = createSelector(
  [getChoreMap],
  choreMap => choreMap.toList(),
);

export default {
  getIsLoading,
  getChoreMap,
  getChores,
};
