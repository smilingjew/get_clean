import { Record } from 'immutable';

const Chore = Record({
  id: null,
  name: '',
});

export default {
  Chore,
};
