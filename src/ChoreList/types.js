const PREFIX = '$CHORE_LIST/';

export default {
  SET_CHORE_MAP: `${PREFIX}SET_CHORE_MAP`,
};
