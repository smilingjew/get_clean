import { Map } from 'immutable';
import types from 'ChoreList/types';
import models from 'ChoreList/models';

const setChoreMap = choreMap => ({
  type: types.SET_CHORE_MAP,
  payload: {
    choreMap,
  },
});

const init = () => async (dispatch, getState, api) => {
  const resp = await api.getChores();
  let choreMap = resp.data.map(chore => [chore.id, new models.Chore(chore)]);
  choreMap = Map(choreMap);
  dispatch(setChoreMap(choreMap));
};

export default {
  init,
};
