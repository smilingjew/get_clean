import React from 'react';
import PropTypes from 'prop-types';
import { Segment, Button, Grid } from 'semantic-ui-react';
import models from 'ChoreList/models';

const ChoreInfo = ({
  chore,
  onClick,
}) => (
  <Segment>
    <Grid>
      <Grid.Row>
        <Grid.Column width="8" verticalAlign="middle">
          {chore.name}
        </Grid.Column>
        <Grid.Column width="8" textAlign="right" verticalAlign="middle">
          <Button
            content="Edit"
            onClick={() => { onClick(chore); }}
          />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  </Segment>
);

ChoreInfo.propTypes = {
  chore: PropTypes.instanceOf(models.Chore).isRequired,
  onClick: PropTypes.func.isRequired,
};

export default ChoreInfo;
