export default {
  '/': { needsAuth: true },
  '/chores/:choreId': { needsAuth: true },
  '/upcoming/:choreInstanceId': { needsAuth: true },
  '/login': {},
  '/register': {},
};
