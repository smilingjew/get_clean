/* global document */
import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { routerForBrowser } from 'redux-little-router';
import App from 'App';
import createStore from 'store';
import api from 'api';
import '../node_modules/semantic-ui-css/semantic.css';
import './main.scss';

const store = createStore(routerForBrowser, api);

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('app'),
);
