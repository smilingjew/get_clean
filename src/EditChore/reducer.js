import { Record, Map } from 'immutable';
import types from 'EditChore/types';

const State = Record({
  loading: true,
  chore: null,
  errors: Map(),
  showDeleteWarning: false,
});

export default (state = new State(), action) => {
  switch (action.type) {
    case types.SET_CHORE:
      return state.withMutations((s) => {
        const { chore } = action.payload;
        return s.set('chore', chore)
          .set('loading', false)
          .set('errors', Map());
      });
    case types.EDIT_CHORE_VALUE:
      return state.withMutations((s) => {
        const {
          fieldName,
          value,
        } = action.payload;
        return s.setIn(['chore', fieldName], value);
      });
    case types.SET_ERRORS:
      return state.withMutations((s) => {
        const { errors } = action.payload;
        return s.set('errors', errors);
      });
    case types.SET_SHOW_DELETE:
      return state.withMutations((s) => {
        const { show } = action.payload;
        return s.set('showDeleteWarning', show);
      });
    default:
      return state;
  }
};
