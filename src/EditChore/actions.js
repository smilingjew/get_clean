import { fromJS } from 'immutable';
import { push } from 'redux-little-router';
import notificationActions from 'Notifications/actions';
import types from 'EditChore/types';
import models from 'EditChore/models';
import selectors from 'EditChore/selectors';

const setChore = chore => ({
  type: types.SET_CHORE,
  payload: {
    chore,
  },
});

const editChoreValue = (fieldName, value) => ({
  type: types.EDIT_CHORE_VALUE,
  payload: {
    fieldName,
    value,
  },
});

const setErrors = errors => ({
  type: types.SET_ERRORS,
  payload: {
    errors,
  },
});

const setShowDelete = show => ({
  type: types.SET_SHOW_DELETE,
  payload: {
    show,
  },
});

const saveChore = () => async (dispatch, getState, api) => {
  dispatch(setErrors(fromJS({})));
  const chore = selectors.getChore(getState());
  let response;
  let apiMethod;
  if (chore.id) {
    apiMethod = api.updateChore;
  } else {
    apiMethod = api.saveChore;
  }
  try {
    response = await apiMethod(chore);
  } catch (error) {
    ({ response } = error);
  }
  if (response.status === 422) {
    dispatch(setErrors(fromJS(response.data.error)));
  } else {
    dispatch(notificationActions.createNotification('Chore created.'));
    dispatch(push('/'));
  }
};

const init = () => async (dispatch, getState, api) => {
  const state = getState();
  const { router } = state;
  const { choreId } = router.params;
  if (choreId === 'new') {
    dispatch(setChore(new models.Chore()));
  } else {
    const resp = await api.getChore(choreId);
    dispatch(setChore(new models.Chore(resp.data)));
  }
};

const deleteChore = () => async (dispatch, getState, api) => {
  const state = getState();
  const chore = selectors.getChore(state);
  await api.deleteChore(chore.id);
  dispatch(notificationActions.createNotification('Chore deleted.'));
  dispatch(push('/'));
};

export default {
  init,
  editChoreValue,
  saveChore,
  setShowDelete,
  deleteChore,
};
