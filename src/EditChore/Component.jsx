import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { Form, Header, Button } from 'semantic-ui-react';
import Loader from 'components/Loader';
import FormField from 'components/FormField';
import models from 'EditChore/models';

class Component extends React.Component {
  static propTypes = {
    loading: PropTypes.bool.isRequired,
    onInit: PropTypes.func.isRequired,
    isNew: PropTypes.bool.isRequired,
    showDelete: PropTypes.bool.isRequired,
    onValueEdit: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    onShowDeleteClick: PropTypes.func.isRequired,
    onHideDeleteClick: PropTypes.func.isRequired,
    onDeleteClick: PropTypes.func.isRequired,
    errors: PropTypes.instanceOf(Map).isRequired,
    chore: PropTypes.instanceOf(models.Chore),
  }

  static defaultProps = {
    chore: null,
  }

  componentDidMount() {
    this.props.onInit();
  }

  render() {
    const {
      chore,
      loading,
      onValueEdit,
      onSave,
      errors,
      isNew,
      showDelete,
      onShowDeleteClick,
      onHideDeleteClick,
      onDeleteClick,
    } = this.props;

    if (loading) {
      return <Loader />;
    }
    const showDeleteButton = !isNew && !showDelete;
    return (
      <div>
        <Header
          content="Edit Form"
        />
        <Form>
          <FormField
            label="Name"
            fieldName="name"
            value={chore.name}
            onChange={onValueEdit}
            errors={errors.get('name')}
          />
          <FormField
            label="How often?"
            fieldName="frequencyAmount"
            type="number"
            value={chore.frequencyAmount}
            onChange={onValueEdit}
            errors={errors.get('frequencyAmount')}
            placeholder="How often will you do this?"
          />
          <FormField
            fieldName="frequency"
            value={chore.frequency}
            onChange={onValueEdit}
            errors={errors.get('frequency')}
            type="select"
            placeholder="Select frequency"
            options={[
              {
                key: 'days-frequency',
                text: 'Days',
                value: 'days',
              },
              {
                key: 'weeks-frequency',
                text: 'Weeks',
                value: 'weeks',
              },
              {
                key: 'months-frequency',
                text: 'Months',
                value: 'months',
              },
              {
                key: 'years-frequency',
                text: 'Years',
                value: 'years',
              },
            ]}
          />
        </Form>
        <Button
          content="Save"
          onClick={onSave}
        />
        {showDeleteButton ? <Button
          content="Delete"
          onClick={onShowDeleteClick}
        /> : null}
        {showDelete ? (
          <div>
            Are you sure?
            <Button
              content="Cancel"
              onClick={onHideDeleteClick}
            />
            <Button
              content="Delete"
              onClick={onDeleteClick}
            />
          </div>
        ) : null}
      </div>
    );
  }
}

export default Component;
