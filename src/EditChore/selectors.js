import { createSelector } from 'reselect';

const getLoading = ({ editChore }) => editChore.loading;
const getChore = ({ editChore }) => editChore.chore;
const getErrors = ({ editChore }) => editChore.errors;
const getShowDelete = ({ editChore }) => editChore.showDeleteWarning;

const getIsNew = createSelector(
  [getChore],
  chore => chore === null || chore.id === null,
);

export default {
  getLoading,
  getChore,
  getErrors,
  getIsNew,
  getShowDelete,
};
