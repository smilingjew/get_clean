import { connect } from 'react-redux';
import selectors from 'EditChore/selectors';
import actions from 'EditChore/actions';
import Component from 'EditChore/Component';

const mapStateToProps = state => ({
  loading: selectors.getLoading(state),
  chore: selectors.getChore(state),
  errors: selectors.getErrors(state),
  isNew: selectors.getIsNew(state),
  showDelete: selectors.getShowDelete(state),
});

const mapDispatchToProps = dispatch => ({
  onInit: () => { dispatch(actions.init()); },
  onValueEdit: (fieldName, value) => { dispatch(actions.editChoreValue(fieldName, value)); },
  onSave: () => { dispatch(actions.saveChore()); },
  onShowDeleteClick: () => { dispatch(actions.setShowDelete(true)); },
  onHideDeleteClick: () => { dispatch(actions.setShowDelete(false)); },
  onDeleteClick: () => { dispatch(actions.deleteChore()); },
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
