import { Record } from 'immutable';

const Chore = Record({
  id: null,
  name: '',
  frequency: '',
  frequencyAmount: 0,
});

export default {
  Chore,
};
