import axios from 'axios';

let token = null;


const instance = axios.create({
  baseURL: process.env.API_HOST,
  headers: { 'Content-Type': 'application/json' },
});

const logIn = async (email, password) => {
  const resp = await instance.post('/auth/login', {
    email,
    password,
  }, { withCredentials: true });
  ({ token } = resp.data);
  instance.defaults.headers.common.Authorization = `Bearer ${token}`;
  return resp;
};

const registerUser = async (email, password, confirm) => {
  const resp = await instance.post('/auth/register', {
    email,
    password,
    confirm,
  }, { withCredentials: true });
  ({ token } = resp.data);
  instance.defaults.headers.common.Authorization = `Bearer ${token}`;
  return resp;
};

const loadUser = async () => {
  const resp = await instance.get('/auth/user', {
    withCredentials: true,
  });
  ({ token } = resp.data);
  instance.defaults.headers.common.Authorization = `Bearer ${token}`;
  return resp;
};

const getUpcomingChores = async () => instance.get('/api/chore-instances');

const getChoreInstance = async instanceId => instance.get(`/api/chore-instances/${instanceId}`);

const completeChore = async (instanceId, completedDate) => instance.post(`/api/chore-completions/${instanceId}`, { completedDate });

const saveChore = async chore => instance.post('/api/chores', chore);

const updateChore = async chore => instance.post(`/api/chores/${chore.id}`, chore);

const getChores = async () => instance.get('/api/chores');

const getChore = async choreId => instance.get(`/api/chores/${choreId}`);

const deleteChore = async choreId => instance.delete(`/api/chores/${choreId}`);

export default {
  logIn,
  registerUser,
  loadUser,
  getUpcomingChores,
  getChoreInstance,
  completeChore,
  saveChore,
  getChores,
  getChore,
  updateChore,
  deleteChore,
};
