import { Record } from 'immutable';
import types from 'ChoreInstance/types';

const State = Record({
  loading: true,
  chore: null,
});

export default (state = new State(), action) => {
  switch (action.type) {
    case types.SET_CHORE:
      return state.withMutations((s) => {
        const { chore } = action.payload;
        return s.set('chore', chore)
          .set('loading', false);
      });
    default:
      return state;
  }
};
