import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
import Loader from 'components/Loader';
import models from 'ChoreInstance/models';


class Component extends React.Component {
  static propTypes = {
    onInit: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    onCompleteClick: PropTypes.func.isRequired,
    chore: PropTypes.instanceOf(models.Chore),
  }

  static defaultProps = {
    chore: null,
  }

  componentDidMount() {
    this.props.onInit();
  }

  render() {
    const {
      loading,
      chore,
      onCompleteClick,
    } = this.props;
    if (loading) {
      return <Loader />;
    }

    return (
      <div>
        <div>{chore.name}</div>
        <div>{chore.notes}</div>
        <Button
          content="I did it"
          onClick={onCompleteClick}
        />
      </div>
    );
  }
}

export default Component;
