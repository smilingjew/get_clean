import { push } from 'redux-little-router';
import types from 'ChoreInstance/types';
import models from 'ChoreInstance/models';
import selectors from 'ChoreInstance/selectors';

const setChore = chore => ({
  type: types.SET_CHORE,
  payload: {
    chore,
  },
});


const init = () => async (dispatch, getState, api) => {
  const state = getState();
  const instanceId = state.router.params.choreInstanceId;
  try {
    const resp = await api.getChoreInstance(instanceId);
    const chore = new models.Chore(resp.data);
    dispatch(setChore(chore));
  } catch (error) {
    if (error.response.status === 404) {
      dispatch(push('/404'));
    }
  }
};

const completeChore = () => async (dispatch, getState, api) => {
  const state = getState();
  const chore = selectors.getChore(state);
  await api.completeChore(chore.id, new Date());
  dispatch(push('/'));
};

export default {
  init,
  setChore,
  completeChore,
};
