const PREFIX = '@CHORE_INSTANCE/';

export default {
  SET_CHORE: `${PREFIX}SET_CHORE`,
};
