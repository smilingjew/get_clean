const getIsLoading = ({ choreInstance }) => choreInstance.loading;
const getChore = ({ choreInstance }) => choreInstance.chore;

export default {
  getIsLoading,
  getChore,
};
