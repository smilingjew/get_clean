import { connect } from 'react-redux';
import Component from 'ChoreInstance/Component';
import actions from 'ChoreInstance/actions';
import selectors from 'ChoreInstance/selectors';

const mapStateToProps = state => ({
  loading: selectors.getIsLoading(state),
  chore: selectors.getChore(state),
});

const mapDispatchToProps = dispatch => ({
  onInit: () => { dispatch(actions.init()); },
  onCompleteClick: () => { dispatch(actions.completeChore()); },
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
