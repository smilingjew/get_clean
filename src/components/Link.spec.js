/* global test expect jest */
import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import Link from 'components/Link';

test('Link renders content', async () => {
  let component = renderer.create(<Link content="test content" onClick={() => {}} />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  component = renderer.create(<Link content="other content" onClick={() => {}} />);
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('Link fires click event', async () => {
  const mockClick = jest.fn();
  const mockEvent = {
    preventDefault: jest.fn(),
    stopPropagation: jest.fn(),
  };
  const wrapper = shallow(<Link
    content="test"
    onClick={mockClick}
  />);
  wrapper.find('a').simulate('click', mockEvent);
  expect(mockClick.mock.calls.length).toBe(1);
  expect(mockClick.mock.calls[0][0]).toBe(mockEvent);

  expect(mockEvent.preventDefault.mock.calls.length).toBe(1);
  expect(mockEvent.stopPropagation.mock.calls.length).toBe(1);
});
