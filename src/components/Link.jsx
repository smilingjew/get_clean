import React from 'react';
import PropTypes from 'prop-types';

const Link = ({ content, onClick }) => (
  <a
    onClick={(e) => {
      e.preventDefault();
      e.stopPropagation();
      onClick(e);
    }}
  >
    {content}
  </a>
);

Link.propTypes = {
  content: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Link;
