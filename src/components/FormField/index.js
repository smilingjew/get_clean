import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { Form, Message } from 'semantic-ui-react';

const FormField = (props) => {
  const {
    type,
    fieldName,
    onChange,
    errors,
    value,
    ...remaining
  } = props;

  let comp = null;
  let inputVal = value;

  if (value === null) {
    inputVal = '';
  }

  switch (props.type) {
    case 'select':
      comp = (<Form.Select
        error={errors.size > 0}
        value={inputVal}
        onChange={(e, { value: val }) => { onChange(fieldName, val); }}
        {...remaining}
      />);
      break;
    case 'checkbox':
      comp = (
        <Form.Checkbox
          checked={value}
          onChange={(e, { checked }) => { onChange(fieldName, checked); }}
          {...remaining}
        />
      );
      break;
    default:
      comp = (<Form.Input
        error={errors.size > 0}
        onChange={(e, { value: val }) => { onChange(fieldName, val); }}
        value={inputVal}
        {...remaining}
        type={type}
      />);
      break;
  }
  return (
    <div>
      {comp}
      {errors.size > 0 ? <Message
        error
        header="Errors"
        visible
        list={errors.toArray()}
      /> : null }
    </div>
  );
};

FormField.propTypes = {
  fieldName: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.number,
  ]),
  errors: PropTypes.instanceOf(List),
  onChange: PropTypes.func,
  type: PropTypes.oneOf([
    'text',
    'select',
    'checkbox',
    'password',
    'number',
  ]),
  label: PropTypes.string,
};

FormField.defaultProps = {
  type: 'text',
  value: '',
  label: '',
  errors: List(),
  onChange: () => {},
};

export default FormField;
