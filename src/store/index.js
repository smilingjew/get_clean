import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';
import routes from 'routes';
import authMiddleware from 'auth/middleware';
import auth from 'auth/reducer';
import login from 'LogIn/reducer';
import register from 'Register/reducer';
import choreInstance from 'ChoreInstance/reducer';
import upcoming from 'Upcoming/reducer';
import editChore from 'EditChore/reducer';
import choreList from 'ChoreList/reducer';
import notifications from 'Notifications/reducer';

export default (router, api) => {
  const { reducer, middleware, enhancer } = router({
    routes,
  });

  return createStore(
    combineReducers({
      auth,
      login,
      register,
      upcoming,
      choreInstance,
      notifications,
      editChore,
      choreList,
      router: reducer,
    }),
    compose(
      enhancer,
      applyMiddleware(
        thunk.withExtraArgument(api),
        middleware,
        authMiddleware,
      ),
    ),
  );
};
