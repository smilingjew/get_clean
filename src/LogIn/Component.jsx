import React from 'react';
import PropTypes from 'prop-types';
import { Segment, Form, Header } from 'semantic-ui-react';
import { Map } from 'immutable';
import FormField from 'components/FormField';
import Link from 'components/Link';

class Component extends React.Component {
  static propTypes = {
    onRegisterClick: PropTypes.func.isRequired,
    onValueChange: PropTypes.func.isRequired,
    onLoginClick: PropTypes.func.isRequired,
    email: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired,
    errors: PropTypes.instanceOf(Map),
  };

  static defaultProps = {
    errors: Map(),
  }

  componentDidMount() {
  }

  render() {
    const {
      onRegisterClick,
      errors,
      onValueChange,
      onLoginClick,
      email,
      password,
    } = this.props;
    return (
      <Segment>
        <Form>
          <Header
            content="Log In"
          />
          <FormField
            label="Email"
            fieldName="email"
            placeholder="Enter your email..."
            tabIndex="0"
            value={email}
            errors={errors.get('email')}
            onChange={onValueChange}
          />
          <FormField
            label="Password"
            fieldName="password"
            type="password"
            tabIndex="0"
            value={password}
            errors={errors.get('password')}
            onChange={onValueChange}
          />
          <Form.Button
            onClick={onLoginClick}
            content="Log In"
          />
          <Link
            content="New user? Register here."
            onClick={onRegisterClick}
          />
        </Form>
      </Segment>
    );
  }
}

export default Component;
