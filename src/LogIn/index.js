import { connect } from 'react-redux';
import { push } from 'redux-little-router';
import Component from 'LogIn/Component';
import actions from 'LogIn/actions';
import selectors from 'LogIn/selectors';

const mapStateToProps = state => ({
  email: selectors.getEmail(state),
  password: selectors.getPassword(state),
  errors: selectors.getErrors(state),
});

const mapDispatchToProps = dispatch => ({
  onValueChange: (fieldName, value) => { dispatch(actions.setLogInValue(fieldName, value)); },
  onRegisterClick: () => { dispatch(push('/register')); },
  onLoginClick: () => { dispatch(actions.logIn()); },
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
