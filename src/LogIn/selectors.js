const getEmail = ({ login }) => login.email;
const getPassword = ({ login }) => login.password;
const getErrors = ({ login }) => login.errors;

export default {
  getEmail,
  getPassword,
  getErrors,
};
