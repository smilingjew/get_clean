import { fromJS } from 'immutable';
import types from 'LogIn/types';
import selectors from 'LogIn/selectors';
import authActions from 'auth/actions';

const setLogInValue = (fieldName, value) => ({
  type: types.SET_LOGIN_VALUE,
  payload: {
    fieldName,
    value,
  },
});

const setErrors = errors => ({
  type: types.SET_ERRORS,
  payload: {
    errors,
  },
});

const logIn = () => async (dispatch, getState, api) => {
  const state = getState();
  dispatch(setErrors(fromJS({})));
  const email = selectors.getEmail(state);
  const password = selectors.getPassword(state);
  let resp;
  try {
    resp = await api.logIn(email, password);
  } catch (error) {
    ({ response: resp } = error);
  }
  if (resp.status < 300) {
    await dispatch(authActions.loadUser(resp.data));
    return;
  }

  if (resp.data.error) {
    const errors = fromJS({
      email: [resp.data.error],
    });
    dispatch(setErrors(errors));
  }
};

export default {
  setLogInValue,
  logIn,
  setErrors,
};
