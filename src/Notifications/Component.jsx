import React from 'react';
import { List } from 'immutable';
import { Message } from 'semantic-ui-react';
import PropTypes from 'prop-types';

const Components = ({
  notifications,
  onNotificationRemove,
}) => (
  <div className="notifications">
    {
      notifications.map(n => (
        <Message
          content={n.message}
          onDismiss={() => { onNotificationRemove(n.id); }}
          key={n.id}
        />
      ))
    }
  </div>
);

Components.propTypes = {
  notifications: PropTypes.instanceOf(List).isRequired,
  onNotificationRemove: PropTypes.func.isRequired,
};

export default Components;
