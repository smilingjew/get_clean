import { Record, Map } from 'immutable';
import types from 'Notifications/types';

const State = Record({
  notifications: Map(),
});

export default (state = new State(), action) => {
  switch (action.type) {
    case types.ADD_NOTIFICATION:
      return state.withMutations((s) => {
        const { notification } = action.payload;
        return s.setIn(['notifications', notification.id], notification);
      });
    case types.DELETE_NOTIFICATION:
      return state.withMutations((s) => {
        const { notificationId } = action.payload;
        return s.removeIn(['notifications', notificationId]);
      });
    default:
      return state;
  }
};
