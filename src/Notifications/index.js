import { connect } from 'react-redux';
import Component from 'Notifications/Component';
import selectors from 'Notifications/selectors';
import actions from 'Notifications/actions';

const mapStateToProps = state => ({
  notifications: selectors.getNotifications(state),
});

const mapDispatchToProps = dispatch => ({
  onNotificationRemove: (notificationId) => {
    dispatch(actions.removeNotification(notificationId));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
