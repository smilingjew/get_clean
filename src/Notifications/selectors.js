import { createSelector } from 'reselect';

const getNotificationsMap = ({ notifications }) => notifications.notifications;

const getNotifications = createSelector(
  [getNotificationsMap],
  notifications => notifications.toList(),
);

export default {
  getNotifications,
};
