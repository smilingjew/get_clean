import { v4 } from 'uuid';
import types from 'Notifications/types';
import models from 'Notifications/models';

const addNotification = notification => ({
  type: types.ADD_NOTIFICATION,
  payload: {
    notification,
  },
});

const deleteNotification = notificationId => ({
  type: types.DELETE_NOTIFICATION,
  payload: {
    notificationId,
  },
});

const createNotification = message => (dispatch) => {
  const notification = new models.Notification({
    message,
    id: v4(),
    created: new Date(),
  });
  dispatch(addNotification(notification));
};

const removeNotification = notificationId => (dispatch) => {
  dispatch(deleteNotification(notificationId));
};

export default {
  createNotification,
  removeNotification,
};
