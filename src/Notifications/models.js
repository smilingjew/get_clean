import { Record } from 'immutable';

const Notification = Record({
  id: null,
  created: null,
  message: '',
});

export default {
  Notification,
};
